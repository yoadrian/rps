// DOM Elements
    // Score display
const userScoreDisplay = document.querySelector('.user-score');
const computerScoreDisplay = document.querySelector('.computer-score');
const tieScoreDisplay = document.querySelector('.tie-score');
    // Buttons for keyboard play
const rockButton = document.getElementById('rock');
const paperButton = document.getElementById('paper');
const scissorsButton = document.getElementById('scissors');
    // User and computer choice display
const userChoiceDisplay = document.querySelector('.user-choice');
const computerChoiceDisplay = document.querySelector('.computer-choice');
    // Game elements
const getUserChoice = document.querySelectorAll('.rps-button');
const rpsWrapper = document.querySelector('.rps-wrapper');
const playButton = document.querySelector('.play-button');
const gameSection = document.querySelector('.game-section');
const choiceIcons = document.querySelector('.choice-icons');
    // Heading appearing when the game (5 rounds) is over
const gameScoreHeading = document.querySelector('.game-score');
    // Restart button when the game is over
const restartButton = document.querySelector('.restart-game');
    // Audio elements
let gameStartSound = document.querySelector('.game-start');
let roundWonSound = document.querySelector('.round-won');
let roundLostSound = document.querySelector('.round-lost');
let gameWonSound = document.querySelector('.game-won');
let gameLostSound = document.querySelector('.game-lost');

// Global variables
let userInput;
let computerChoice;
let playerScore = 0;
let computerScore = 0;
let tieScore = 0;

// gameSection is hidden until playButton is pressed
gameSection.classList.toggle('hide');

// Play button revealing the game when clicked or on keydown of space bar
let startGame = () => {
    gameSection.classList.toggle('hide');
    playButton.classList.toggle('hide');
    restartButton.classList.toggle('hide');
    gameScoreHeading.classList.toggle('hide');
    playStartSound();
}

playButton.addEventListener('click', startGame);

document.addEventListener('keydown', (e) => {
    if (e.key === ' ' && gameSection.classList.contains('hide')) {
        startGame();
    }
});

// Prompts user for click input and plays one round
let promptUser = () => {
    getUserChoice.forEach((choice) => {
        choice.addEventListener('click', (e) => {
            userInput = e.target.id;
            userChoiceDisplay.src = "./img/" + userInput + ".png";
            playComputer();
            playRound();
            if (playerScore === 5) {
                userWins();
            }
            if (computerScore === 5) {
                computerWins();
            }
            choice.classList.add('scale-up'); // added to enlarge clicked button
            setTimeout(function() {
                choice.classList.remove('scale-up');
                // make sure the transition time in CSS matches the setTimeout in JS
            }, 10);
        })  
    })
}
promptUser()

// Prompts user for keyboard input and plays one round
document.addEventListener ('keydown', (e) => {
    if (e.key === 'r' && playButton.classList.contains('hide') && restartButton.classList.contains('hide')) {
        userInput = 'rock';
        userChoiceDisplay.src = "./img/" + userInput + ".png";
        playComputer();
            playRound();
            if (playerScore === 5) {
                userWins();
            }
            if (computerScore === 5) {
                computerWins();
            }
            rockButton.classList.add('scale-up'); // added to enlarge clicked button
            setTimeout(function() {
                rockButton.classList.remove('scale-up');
                // make sure the transition time in CSS matches the setTimeout in JS
            }, 10);
    }

    if (e.key === 'p' && playButton.classList.contains('hide') && restartButton.classList.contains('hide')) {
        userInput = 'paper';
        userChoiceDisplay.src = "./img/" + userInput + ".png";
        playComputer();
            playRound();
            if (playerScore === 5) {
                userWins();
            }
            if (computerScore === 5) {
                computerWins();
            }
            paperButton.classList.add('scale-up'); // added to enlarge clicked button
            setTimeout(function() {
                paperButton.classList.remove('scale-up');
                // make sure the transition time in CSS matches the setTimeout in JS
            }, 10);
    }

    if (e.key === 's' && playButton.classList.contains('hide') && restartButton.classList.contains('hide')) {
        userInput = 'scissors';
        userChoiceDisplay.src = "./img/" + userInput + ".png";
        playComputer();
            playRound();
            if (playerScore === 5) {
                userWins();
            }
            if (computerScore === 5) {
                computerWins();
            }
            scissorsButton.classList.add('scale-up'); // added to enlarge clicked button
            setTimeout(function() {
                scissorsButton.classList.remove('scale-up');
                // make sure the transition time in CSS matches the setTimeout in JS
            }, 10);
    }
})

// Plays one round of computer choice
let playComputer = () => {
    const randomNumber = Math.floor(Math.random() * getUserChoice.length);
    if (randomNumber === 0) {
        computerChoice = 'rock';
    }
    if (randomNumber === 1) {
        computerChoice = 'paper';
    }
    if (randomNumber === 2) {
        computerChoice = 'scissors'
    }
    computerChoiceDisplay.src = "./img/" + computerChoice + ".png";
}

// Updates the scores for one round, displays them, and triggers sound win/ lost functions
let playRound = () => {
    if (userInput === computerChoice) {
        playerScore = playerScore;
        computerScore = computerScore;
        tieScore++;
    }
    if (userInput === 'rock' && computerChoice === 'paper') {
        computerScore++;
        playRoundLostSound();
    }
    if (userInput === 'rock' && computerChoice === 'scissors') {
        playerScore++;
        playRoundWonSound();
    }
    if (userInput === 'paper' && computerChoice === 'rock') {
        playerScore++;
        playRoundWonSound();
    }
    if (userInput === 'paper' && computerChoice === 'scissors') {
        computerScore++;
        playRoundLostSound();
    }
    if (userInput === 'scissors' && computerChoice === 'rock') {
        computerScore++;
        playRoundLostSound();
    }
    if (userInput === 'scissors' && computerChoice === 'paper') {
        playerScore++;
        playRoundWonSound();
    }
    userScoreDisplay.innerText = playerScore;
    computerScoreDisplay.innerText = computerScore;
    tieScoreDisplay.innerText = tieScore;
}

// Procedure when the user wins
let userWins = () => {
    rpsWrapper.classList.toggle('hide');
    restartButton.classList.toggle('hide');
    choiceIcons.classList.toggle('hide');
    gameScoreHeading.classList.toggle('hide');
    restartButton.innerText = 'You won! Go again!';
    playGameWonSound();
}

// Procedure when the computer wins
let computerWins = () => {
    rpsWrapper.classList.toggle('hide');
    restartButton.classList.toggle('hide');
    choiceIcons.classList.toggle('hide');
    gameScoreHeading.classList.toggle('hide');
    restartButton.innerText = 'You lost! Go again!';
    playGameLostSound();
}

// Function to restart the game and hide/ show elements on click event
let restartGame = () => {
    restartButton.addEventListener('click', () => {
        playerScore = 0;
        computerScore = 0;
        tieScore = 0;
        userChoiceDisplay.src = './img/question.png';
        computerChoiceDisplay.src = './img/question.png';
        userScoreDisplay.innerText = playerScore;
        computerScoreDisplay.innerText = computerScore;
        tieScoreDisplay.innerText = tieScore;
        rpsWrapper.classList.toggle('hide');
        restartButton.classList.toggle('hide');
        choiceIcons.classList.toggle('hide');
        gameScoreHeading.classList.toggle('hide');
        playStartSound();
        // Pause sounds when restart
        gameWonSound.pause();
        gameLostSound.pause();
    })
}
restartGame()

// Function to restart the game and hide/ show elements on keydown event
document.addEventListener('keydown', (e) => {
    if (e.key === ' ' & choiceIcons.classList.contains('hide')) {
        playerScore = 0;
        computerScore = 0;
        tieScore = 0;
        userChoiceDisplay.src = './img/question.png';
        computerChoiceDisplay.src = './img/question.png';
        userScoreDisplay.innerText = playerScore;
        computerScoreDisplay.innerText = computerScore;
        tieScoreDisplay.innerText = tieScore;
        rpsWrapper.classList.toggle('hide');
        restartButton.classList.toggle('hide');
        choiceIcons.classList.toggle('hide');
        gameScoreHeading.classList.toggle('hide');
        playStartSound();
        // Pause sounds when restart
        gameWonSound.pause();
        gameLostSound.pause();
    }
});

// Sound when start/ restart game
let playStartSound = () => {
    gameStartSound.play();
}

// Sound when the user wins the round
// currentTime added because rps buttons are played in fast succession
let playRoundWonSound = () => {
    roundWonSound.currentTime = 0;
    roundWonSound.play();
}

// Sound when computer wins the round
// currentTime added because rps buttons are played in fast succession
let playRoundLostSound = () => {
    roundLostSound.currentTime = 0; 
    roundLostSound.play();
}

// Sound when the whole game is won by player
// currentTime to reset paused sound when replay button clicked
let playGameWonSound = () => {
    gameWonSound.currentTime = 0; 
    gameWonSound.play();
}

// Sound when the whole game is lost by player
let playGameLostSound = () => {
    gameLostSound.currentTime = 0;
    gameLostSound.play();
}