# Rock Paper Scissors UI
Play game at https://yoadrian.gitlab.io/rps/ 

## Project Objectives
Game Objectives
- player should be able to play the game by clicking on buttons rather than typing their answer in a prompt
- create three buttons, one for each selection. Add an event listener to the buttons that call your playRound function with the correct playerSelection every time a button is clicked.
- add a div for displaying results
- display the running score, and announce a winner of the game once one player reaches 5 points
- add a live preview link to a project page

Meta Objectives
- learn and use Git branches
- learn and use GitLab's project management features (Issues tab)

## How I Approached Building the Project
I planned the project using these steps:
- Information gathering
    - I started by researching and creating a document of reseources for the entire project: resources.txt
- Planning
    - created two 1-week milestones
    - created project labels and boards for main activities: Code, Design, Research, QA
- Design
    - created app wireframe
    - created image file to draw color palette from
    - used Adobe Color Wheel to produce a color palette for app
- Code
    - created working app in main branch
    - created branch rps-sound to add sound features, merged into main and deleted the branch
    - created branch rps-kbd to add keyboard features, merged into main and deleted the branch
- Test
    - tested by myself using various scenarios
- Launch
    - created a GitLab page for app
- Review/ Community Review
    - shared for feedback in various communities
- Maintenance

## Tools/ Tehcnologies Used
Git/ Gitlab
- Git: create/ clone/ push/ branch/ checkout branch/ merge branch/ delete branch
- GitLab's Issues tab
 - Epics, Roadmap, Scoped Labels (not available in the free account)
 - Milestones: created and managed issues around milestones
 - Labels: created and assgned labels to issues and board-lists
 - Boards: created and assigned lists to board

LibreOffice Draw, Penpot: to create a wireframe

Colordesigner.io and color.adobe.com/create/color-wheel to create color palette
